<?php

/* default/template/common/menu.twig */
class __TwigTemplate_79ca4db9473e1fa7e14c45ec298721f9eead8320609fa1a1453a552f036bb167 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["categories"]) ? $context["categories"] : null)) {
            // line 2
            echo "<div class=\"yamm navbar navbar-default\" role=\"navigation\">
    <div class=\"navbar-header\">
        <button data-target=\"#mc-horizontal-menu-collapse\" data-toggle=\"collapse\" class=\"navbar-toggle collapsed\" type=\"button\">
            <span class=\"sr-only\">Toggle navigation</span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span> </button>
    </div>
    <div class=\"nav-bg-class\">
        <div class=\"navbar-collapse collapse\" id=\"mc-horizontal-menu-collapse\">
            <div class=\"nav-outer\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"dropdown\"> <a href=\"";
            // line 11
            echo (isset($context["home"]) ? $context["home"] : null);
            echo "\">";
            echo (isset($context["text_home"]) ? $context["text_home"] : null);
            echo "</a> </li>
                    ";
            // line 12
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 13
                echo "                        ";
                if ($this->getAttribute($context["category"], "children", array())) {
                    // line 14
                    echo "                            <li class=\"dropdown\">
                                <a href=\"";
                    // line 15
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\"  data-hover=\"dropdown\">";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "</a>
                                <ul class=\"dropdown-menu\">
                                    <li>
                                        <div class=\"yamm-content\">
                                            <div class=\"row\">
                                                <div class=\"col-xs-12 col-menu\">
                                                    <ul class=\"links\">
                                                        ";
                    // line 22
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["category"], "children", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                        // line 23
                        echo "                                                            <li><a href=\"";
                        echo $this->getAttribute($context["child"], "href", array());
                        echo "\">";
                        echo $this->getAttribute($context["child"], "name", array());
                        echo "</a></li>
                                                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 25
                    echo "                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                        <!-- /.yamm-content -->
                                    </li>
                                </ul>
                            </li>
                        ";
                } else {
                    // line 35
                    echo "                            <li class=\"dropdown\"> <a href=\"";
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "</a> </li>
                    ";
                }
                // line 37
                echo "                        
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 39
            echo "                    ";
            if ((isset($context["categories_news"]) ? $context["categories_news"] : null)) {
                // line 40
                echo "                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["categories_news"]) ? $context["categories_news"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                    // line 41
                    echo "                            ";
                    if ($this->getAttribute($context["category"], "children", array())) {
                        // line 42
                        echo "                                <li class=\"dropdown mega-menu\">
                                    <a href=\"";
                        // line 43
                        echo $this->getAttribute($context["category"], "href", array());
                        echo "\"  data-hover=\"dropdown\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                        echo $this->getAttribute($context["category"], "name", array());
                        echo "</a>
                                    <ul class=\"dropdown-menu container\">
                                        <li>
                                            <div class=\"yamm-content\">
                                                <div class=\"row\">
                                                    ";
                        // line 48
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_array_batch($this->getAttribute($context["category"], "children", array()), (twig_length_filter($this->env, $this->getAttribute($context["category"], "children", array())) / twig_round($this->getAttribute($context["category"], "column", array()), 1, "ceil"))));
                        foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                            // line 49
                            echo "                                                        <div class=\"col-xs-12 col-sm-12 col-md-3 col-menu\">
                                                            <h2 class=\"title\">";
                            // line 50
                            echo $this->getAttribute($context["category"], "name", array());
                            echo "</h2>
                                                            <ul class=\"links\">
                                                                ";
                            // line 52
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable($context["children"]);
                            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                                // line 53
                                echo "                                                                    <li><a href=\"";
                                echo $this->getAttribute($context["child"], "href", array());
                                echo "\">";
                                echo $this->getAttribute($context["child"], "name", array());
                                echo "</a></li>
                                                                ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 55
                            echo "                                                            </ul>
                                                        </div>
                                                        <!-- /.col -->
                                                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 59
                        echo "                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.yamm-content -->
                                        </li>
                                    </ul>
                                </li>
                            ";
                    } else {
                        // line 67
                        echo "                                <li class=\"dropdown\"> <a href=\"";
                        echo $this->getAttribute($context["category"], "href", array());
                        echo "\">";
                        echo $this->getAttribute($context["category"], "name", array());
                        echo "</a> </li>
                            ";
                    }
                    // line 69
                    echo "                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 70
                echo "                    ";
            }
            echo "                    
                    <li class=\"dropdown\"> <a href=\"";
            // line 71
            echo (isset($context["contact"]) ? $context["contact"] : null);
            echo "\">";
            echo (isset($context["text_contact"]) ? $context["text_contact"] : null);
            echo "</a> </li>
                </ul>
                <!-- /.navbar-nav -->
                <div class=\"clearfix\"></div>
            </div>
            <!-- /.nav-outer -->
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.nav-bg-class -->
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "default/template/common/menu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 71,  186 => 70,  180 => 69,  172 => 67,  162 => 59,  153 => 55,  142 => 53,  138 => 52,  133 => 50,  130 => 49,  126 => 48,  116 => 43,  113 => 42,  110 => 41,  105 => 40,  102 => 39,  95 => 37,  87 => 35,  75 => 25,  64 => 23,  60 => 22,  48 => 15,  45 => 14,  42 => 13,  38 => 12,  32 => 11,  21 => 2,  19 => 1,);
    }
}
/* {% if categories %}*/
/* <div class="yamm navbar navbar-default" role="navigation">*/
/*     <div class="navbar-header">*/
/*         <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">*/
/*             <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>*/
/*     </div>*/
/*     <div class="nav-bg-class">*/
/*         <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">*/
/*             <div class="nav-outer">*/
/*                 <ul class="nav navbar-nav">*/
/*                     <li class="dropdown"> <a href="{{ home }}">{{ text_home }}</a> </li>*/
/*                     {% for category in categories %}*/
/*                         {% if category.children %}*/
/*                             <li class="dropdown">*/
/*                                 <a href="{{ category.href }}"  data-hover="dropdown">{{ category.name }}</a>*/
/*                                 <ul class="dropdown-menu">*/
/*                                     <li>*/
/*                                         <div class="yamm-content">*/
/*                                             <div class="row">*/
/*                                                 <div class="col-xs-12 col-menu">*/
/*                                                     <ul class="links">*/
/*                                                         {% for child in category.children %}*/
/*                                                             <li><a href="{{ child.href }}">{{ child.name }}</a></li>*/
/*                                                         {% endfor %}*/
/*                                                     </ul>*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                             <!-- /.row -->*/
/*                                         </div>*/
/*                                         <!-- /.yamm-content -->*/
/*                                     </li>*/
/*                                 </ul>*/
/*                             </li>*/
/*                         {% else %}*/
/*                             <li class="dropdown"> <a href="{{ category.href }}">{{ category.name }}</a> </li>*/
/*                     {% endif %}*/
/*                         */
/*                     {% endfor %}*/
/*                     {% if categories_news %}*/
/*                         {% for category in categories_news %}*/
/*                             {% if category.children %}*/
/*                                 <li class="dropdown mega-menu">*/
/*                                     <a href="{{ category.href }}"  data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown">{{ category.name }}</a>*/
/*                                     <ul class="dropdown-menu container">*/
/*                                         <li>*/
/*                                             <div class="yamm-content">*/
/*                                                 <div class="row">*/
/*                                                     {% for children in category.children|batch(category.children|length / category.column|round(1, 'ceil')) %}*/
/*                                                         <div class="col-xs-12 col-sm-12 col-md-3 col-menu">*/
/*                                                             <h2 class="title">{{ category.name }}</h2>*/
/*                                                             <ul class="links">*/
/*                                                                 {% for child in children %}*/
/*                                                                     <li><a href="{{ child.href }}">{{ child.name }}</a></li>*/
/*                                                                 {% endfor %}*/
/*                                                             </ul>*/
/*                                                         </div>*/
/*                                                         <!-- /.col -->*/
/*                                                     {% endfor %}*/
/*                                                 </div>*/
/*                                                 <!-- /.row -->*/
/*                                             </div>*/
/*                                             <!-- /.yamm-content -->*/
/*                                         </li>*/
/*                                     </ul>*/
/*                                 </li>*/
/*                             {% else %}*/
/*                                 <li class="dropdown"> <a href="{{ category.href }}">{{ category.name }}</a> </li>*/
/*                             {% endif %}*/
/*                         {% endfor %}*/
/*                     {% endif %}                    */
/*                     <li class="dropdown"> <a href="{{ contact }}">{{ text_contact }}</a> </li>*/
/*                 </ul>*/
/*                 <!-- /.navbar-nav -->*/
/*                 <div class="clearfix"></div>*/
/*             </div>*/
/*             <!-- /.nav-outer -->*/
/*         </div>*/
/*         <!-- /.navbar-collapse -->*/
/*     </div>*/
/*     <!-- /.nav-bg-class -->*/
/* </div>*/
/* {% endif %}*/
